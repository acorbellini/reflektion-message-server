CREATE TABLE IF NOT EXISTS message (
 id int(11) NOT NULL AUTO_INCREMENT,
 country varchar(255) DEFAULT NULL,
 ip varchar(255) DEFAULT NULL,
 lang varchar(255) DEFAULT NULL,
 message varchar(255) DEFAULT NULL,
 post_date datetime DEFAULT NULL,
 PRIMARY KEY (id)
);
