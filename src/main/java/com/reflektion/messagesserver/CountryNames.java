package com.reflektion.messagesserver;

import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CountryNames {

	private static final CountryNames current = new CountryNames();

	public static CountryNames instance() {
		return current;
	}

	private HashMap<String, String> countries;

	@SuppressWarnings("unchecked")
	private CountryNames() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			this.countries = mapper.readValue(getClass().getResource("/names.json"), HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getCountryByCode(String code) {
		return countries.get(code);
	}
}
