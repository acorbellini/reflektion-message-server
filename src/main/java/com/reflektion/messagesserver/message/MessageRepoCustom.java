package com.reflektion.messagesserver.message;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepoCustom {
	public List<Message> findMessagesByLangLimitBy(String lang, Integer numOf);
}
