package com.reflektion.messagesserver.message;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;

public class MessageRepoImpl implements MessageRepoCustom {
	@Autowired
	EntityManager em;

	public List<Message> findMessagesByLangLimitBy(String lang, Integer numOf) {
		Query query = em
				.createQuery("select msg from Message msg where ?1='all' or msg.lang=?1 order by postDate desc");
		query.setParameter(1, lang);
		query.setMaxResults(numOf);
		return query.getResultList();
	}
}
