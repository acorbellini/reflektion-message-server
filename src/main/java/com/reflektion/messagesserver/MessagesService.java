package com.reflektion.messagesserver;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.knallgrau.utils.textcat.TextCategorizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.maxmind.geoip.LookupService;
import com.reflektion.messagesserver.message.Message;
import com.reflektion.messagesserver.message.MessageRepo;

/**
 * @author Alejandro Corbellini
 *
 */
@Service
public class MessagesService {

	public static final String UNKNOWN_COUNTRY = "Unknown Country";

	@Autowired
	MessageRepo msgRepo;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private CountryNames countryNames = CountryNames.instance();

	private LookupService geoip;

	private TextCategorizer guesser;

	public MessagesService(MessageRepo repo) {
		init();
		this.msgRepo = repo;
	}

	@PostConstruct
	private void init() {
		try {
			// GeoIP uses a File object, so the database must be in the projects
			// root path.
			geoip = new LookupService(new File("./GeoIP.dat"),
					LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);

			// This key is found in detect language account config.
			DetectLanguage.apiKey = "b5a9c8ed63f739be87581fe03cd73108";

			this.guesser = new TextCategorizer();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Message> getMessages(String lang, Integer numOf) {
		return msgRepo.findMessagesByLangLimitBy(lang, numOf);
	}

	public String saveMessage(HttpServletRequest req, String message) {
		String ipAddr = resolveIp(req);
		String country = resolveCountry(ipAddr);
		String lang = resolveLanguage(message, req);
		Message msg = new Message();
		msg.setMessage(message);
		msg.setCountry(country);
		msg.setLang(lang);
		msg.setIp(ipAddr);
		msg.setPostDate(new Date());

		msgRepo.save(msg);

		if (log.isDebugEnabled())
			log.debug(String.format("Message %s received from %s, ip: %s, lang: %s.", message, country, ipAddr, lang));

		return String.format("%s from %s!", message, country);
	}

	private String resolveLanguage(String msg, HttpServletRequest req) {
		// Tries to find the language from the message content. Uses
		// DetectLanguage API in the first place.
		// Then it tries to use TextCategorizer. Finally, if both approaches
		// fail, it returns the accept-language header.
		try {
			List<Result> results = DetectLanguage.detect(msg);

			Result result = results.get(0);
			if (log.isDebugEnabled()) {
				log.debug("Language: " + result.language);
				log.debug("Is reliable: " + result.isReliable);
				log.debug("Confidence: " + result.confidence);
			}
			// if (result.isReliable)
			return result.language;
		} catch (APIError e) {
			e.printStackTrace();
		}

		String category = guesser.categorize(msg);
		if (category != null && !category.isEmpty() && !category.equalsIgnoreCase("unknown"))
			return category.substring(0, Math.min(2, category.length()));

		return req.getHeader("Accept-Language");
	}

	private static final String[] IP_HEADER_CANDIDATES = { "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
			"HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED", "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP",
			"HTTP_FORWARDED_FOR", "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR" };

	private String resolveIp(HttpServletRequest req) {
		// Check possibly forwarded request using headers (e.g. if behind a
		// cloud proxy)
		for (String header : IP_HEADER_CANDIDATES) {
			String ip = req.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}
		// Otherwise, return the remote address.
		return req.getRemoteAddr();
	}

	private String resolveCountry(String ipAddr) {
		if (ipAddr != null) {
			// Uses ipinfo api. If not found, uses GeoIP database (could be out
			// of
			// date).
			try {
				String code = Unirest.get("http://ipinfo.io/{ipAddr}/country").routeParam("ipAddr", ipAddr).asString()
						.getBody();
				String name = countryNames.getCountryByCode(code);
				if (name != null && !name.isEmpty() && !name.equals("N/A"))
					return name;
			} catch (UnirestException e) {
				e.printStackTrace();
			}
			String name = geoip.getCountry(ipAddr).getName();
			if (!name.equals("N/A"))
				return name;
		}
		return UNKNOWN_COUNTRY;

	}

}
