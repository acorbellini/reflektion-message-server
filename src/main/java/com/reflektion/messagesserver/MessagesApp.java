package com.reflektion.messagesserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

@SpringBootApplication
@ActiveProfiles("dev")
public class MessagesApp {
	public static void main(String[] args) {
		SpringApplication.run(MessagesApp.class, args);
	}
}
