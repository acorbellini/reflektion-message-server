package com.reflektion.messagesserver;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.reflektion.messagesserver.message.Message;

/**
 * @author Alejandro Corbellini
 * 
 *         A restful service that stores messages and allows to query stored
 *         messages. It detects the message language and the country of origin
 *         of the request using different tools. The getMessages endpoint
 *         returns the list of messages stored by language and allows to limit
 *         the size of the resulting list.
 * 
 */
@RestController
public class MessagesController {
	private static final String LANG_DEFAULT = "all";
	private static final Integer MAX_MSGS = 1000;

	@Autowired
	MessagesService msgSvc;

	/**
	 * Allows the user to store a arbitrary message received in the request
	 * body.
	 * 
	 * @param req
	 *            The HttpServletRequest from the container (injected by Spring)
	 * @param message
	 *            The message that the user wants to store. It must be part of
	 *            the request body and posted using plain text.
	 * @return greeting A message with the format
	 *         "{@code message} from COUNTRY!"
	 */
	@RequestMapping(value = "/postMessage", method = RequestMethod.POST, consumes = "text/plain")
	public ResponseEntity<String> postMessage(HttpServletRequest req, @RequestBody String message) {
		String saveMessage;
		try {
			saveMessage = msgSvc.saveMessage(req, message);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(saveMessage, HttpStatus.CREATED);
	}

	/**
	 * 
	 * Obtains stored messages.
	 * 
	 * @param lang
	 *            The language to find. Set the language as "all" to return all
	 *            messages, regardless of their language.
	 * @param numOf
	 *            The number of messages to return.
	 * @return A list of messages that match the query parameters.
	 */
	@RequestMapping(value = "/getMessages", method = RequestMethod.GET)
	public List<Message> getMessages(@PathParam("lang") String lang, @PathParam("numOf") Integer numOf) {

		numOf = numOf != null && numOf > 0 && numOf <= MAX_MSGS ? numOf : MAX_MSGS;
		lang = lang != null && lang != "" ? lang : LANG_DEFAULT;

		return msgSvc.getMessages(lang, numOf);

	}
}
