package com.reflektion.messagesserver.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.reflektion.messagesserver.message.Message;
import com.reflektion.messagesserver.message.MessageRepo;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MessageRepoTest {
	@Autowired
	MessageRepo repo;

	@Before
	public void setUp() {
		repo.deleteAll();

		List<Message> messages = new ArrayList<>();

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		String[] countries = new String[] { "Argentina", "Spain", "United States", "Portugal", "South Korea", "Japan" };
		String[] langs = new String[] { "es", "es", "es", "en", "pt", "ko" };
		String[] dates = new String[] { "01-02-2009 13:10", "31-12-2001 00:00", "01-01-2014 00:01", "05-06-2010 12:00",
				"1-1-2016 09:30", "05-06-2010 12:01" };
		for (int i = 0; i < countries.length; i++) {
			Message msg = new Message();
			msg.setCountry(countries[i]);
			msg.setLang(langs[i]);
			try {
				msg.setPostDate(formatter.parse(dates[i]));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			msg.setMessage("Test Message");
			messages.add(msg);
		}

		for (Message message : messages) {
			repo.save(message);
		}
	}

	@Test
	public void testFindByLang() {
		assertThat(repo.findMessagesByLangLimitBy("es", 100).size()).isEqualTo(3);
		assertThat(repo.findMessagesByLangLimitBy("ko", 100).size()).isEqualTo(1);
	}

	@Test
	public void testFindBySize() {
		assertThat(repo.findMessagesByLangLimitBy("all", 1).size()).isEqualTo(1);
		assertThat(repo.findMessagesByLangLimitBy("all", 2).size()).isEqualTo(2);
		assertThat(repo.findMessagesByLangLimitBy("all", 3).size()).isEqualTo(3);

		assertThat(repo.findMessagesByLangLimitBy("es", 2).size()).isEqualTo(2);
	}

	@Test
	public void testSorted() {
		List<Message> findMessagesByLangLimitBy = repo.findMessagesByLangLimitBy("all", 100);
		Message before = findMessagesByLangLimitBy.get(0);
		for (Message msg : findMessagesByLangLimitBy) {
			assertThat(msg.getPostDate()).isBeforeOrEqualsTo(before.getPostDate());
			before = msg;
		}
	}
}
