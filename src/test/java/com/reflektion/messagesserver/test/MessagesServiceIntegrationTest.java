package com.reflektion.messagesserver.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.reflektion.messagesserver.message.Message;
import com.reflektion.messagesserver.message.MessageRepo;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class MessagesServiceIntegrationTest {
	private static final String TEST_MSG = "Dos cosas son infinitas: la estupidez humana y el universo; y no estoy seguro de lo segundo";

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private MessageRepo repo;

	@Before
	public void setUp() {
		repo.deleteAll();
	}

	@Test
	public void postTest() {
		ResponseEntity<String> responseEntity = postMessage();

		String client = responseEntity.getBody();
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		assertEquals(String.format("%s from %s!", TEST_MSG, "United States"), client);
	}

	private ResponseEntity<String> postMessage() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		headers.set("X-Forwarded-For", "8.8.8.8");
		HttpEntity<String> entity = new HttpEntity<String>(TEST_MSG, headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange("/postMessage", HttpMethod.POST, entity,
				String.class);
		return responseEntity;
	}

	@Test
	public void getTest() {
		postMessage();
		ResponseEntity<List> res = restTemplate.getForEntity("/getMessages", List.class, "lang", "es", "numOf", 2);
		List<Message> msgs = res.getBody();
		assertThat(msgs.size()).isEqualTo(1);
	}
}
