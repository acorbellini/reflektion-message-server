package com.reflektion.messagesserver.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.reflektion.messagesserver.MessagesService;
import com.reflektion.messagesserver.message.Message;
import com.reflektion.messagesserver.message.MessageRepo;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessagesServiceTest {
	MessagesService service;

	@Mock
	MessageRepo repo;

	@Mock
	EntityManager em;

	@Before
	public void setUp() {
		repo.deleteAll();
		service = new MessagesService(repo);
	}

	@Test
	public void countryDetectionTest() {
		String[] ips = new String[] { null, "-1", "8.8.8.8", "201.252.75.26", "0.0.0.0", "2001:4860:4860::8888",
				"131.111.15.163", "178.236.6.247" };
		String[] countries = new String[] { MessagesService.UNKNOWN_COUNTRY, MessagesService.UNKNOWN_COUNTRY,
				"United States", "Argentina", MessagesService.UNKNOWN_COUNTRY, "United States", "United Kingdom",
				"Ireland" };
		String message = "Country Detection Test";
		for (int i = 0; i < ips.length; i++) {
			MockHttpServletRequest req = new MockHttpServletRequest();
			req.setRemoteAddr(ips[i]);
			assertThat(service.saveMessage(req, message))
					.isEqualTo(String.format("%s from %s!", message, countries[i]));
		}

	}

	@Test
	public void languageDetectionTest() {
		Map<String, String> messages = new HashMap<>();
		messages.put("Winter is coming", "en");
		messages.put("Viene el invierno", "es");
		messages.put("O inverno está chegando", "pt");
		messages.put("겨울이오고있다", "ko");
		messages.put("Der Winter kommt", "de");

		final List<Message> savedMessages = new ArrayList<>();
		given(repo.save(any(Message.class))).will(new Answer<Message>() {
			@Override
			public Message answer(InvocationOnMock invocation) throws Throwable {
				Message msg = (Message) invocation.getArguments()[0];
				savedMessages.add(msg);
				return msg;
			}
		});
		given(repo.findMessagesByLangLimitBy("all", -1)).willReturn(savedMessages);

		for (Entry<String, String> entry : messages.entrySet()) {
			MockHttpServletRequest req = new MockHttpServletRequest();
			service.saveMessage(req, entry.getKey());
		}

		for (Message message : service.getMessages("all", -1)) {
			String content = message.getMessage();
			assertThat(message.getLang()).isEqualTo(messages.get(content));
		}

	}
}
