package com.reflektion.messagesserver.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.reflektion.messagesserver.MessagesController;
import com.reflektion.messagesserver.MessagesService;
import com.reflektion.messagesserver.message.Message;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest(MessagesController.class)
public class MessagesControllerTests {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private MessagesService svc;

	@Before
	public void setUp() {
		List<Message> msgs = new ArrayList<>();
		Message msg = new Message();
		msg.setMessage("Test Message");
		msg.setCountry("Test Country");
		msg.setIp("1.2.3.4");
		msg.setLang("es");
		msg.setPostDate(new Date());
		msgs.add(msg);

		when(svc.getMessages(any(String.class), any(Integer.class))).thenReturn(msgs);
		when(svc.saveMessage(any(HttpServletRequest.class), any(String.class))).thenReturn("Test Return Message");
	}

	@Test
	public void testPostMessage() throws Exception {
		mvc.perform(post("/postMessage").contentType(MediaType.TEXT_PLAIN).content("Winter is coming"))
				.andExpect(status().isCreated()).andExpect(content().string("Test Return Message"));
	}

	@Test
	public void testGetMessages() throws Exception {
		MvcResult res = mvc.perform(get("/getMessages").param("lang", "all").param("numOf", "1"))
				.andExpect(status().isOk()).andReturn();
		String content = res.getResponse().getContentAsString();

		ObjectMapper mapper = new ObjectMapper();
		TypeFactory factory = mapper.getTypeFactory();
		CollectionType listType = factory.constructCollectionType(List.class, Message.class);
		List<Message> msgs = mapper.readValue(content, listType);

		assertThat(msgs.size()).isEqualTo(1);
		assertThat(msgs.get(0).getMessage()).isEqualTo("Test Message");
	}
}
